## File structure

/`package_id`/  
├─[`meta.json`](meta.md)  
├─[`icon.png`](#icon.png)  
├─[`characters`](#characters)  
└─[`fruits`](#fruits)

The top-level directory must have the same name as the package ID. Be careful
when picking an ID, because it must be unique among **all** packages the user
has installed in their game.

Example structure for a package with ID `package_id` that contains two fruits,
`fruit1` and `fruit2`, two characters, `character1` (doesn't have an item) and
`character2` (has an item).

```
/
└─package_id/
  ├─icon.png
  ├─meta.json
  ├─characters/
  │ ├─character1/
  │ │ └─character.png
  │ └─character2/
  │   ├─character.png
  │   ├─item.png
  │   ├─item_done.png
  │   └─item_using.png
  └─fruits/
    ├─fruit1/
    │ ├─0.png
    │ ├─1.png
    │ ├─2.png
    │ └─3.png
    └─fruit2/
      ├─0.png
      ├─1.png
      ├─2.png
      └─3.png
```

### meta.json

This file must be present.

See [meta.md](meta.md) for information on this file.

### icon.png

This file must be present.

It is a 512 ⨯ 512 PNG image that is the package's icon.

### characters/

This folder must be present if the package includes any characters, and holds
their resources.

For each character, there must be a folder with the same name as the character
ID containing the following files:

| Name             | Size       | Description                                                                                           |
| ---------------- | ---------- | ----------------------------------------------------------------------------------------------------- |
| `character.png`  | 600 ⨯ 1000 | The character to display in-game.                                                                     |
| `item.png`       | 500 ⨯ 500  | The character's item when it is not being used.                                                       |
| `item_done.png`  | 500 ⨯ 500  | The character's item when it is not being used with an indicator to show that the item has been used. |
| `item_using.png` | 500 ⨯ 500  | The character's item when it is being used (after the user has clicked it).                           |

If the character does **not** have an item, only the `character.png` file must
be present, and all others must **not** be present.

If the character **does** have an item, **all** files must be present.

Example structure, where character ID `character1` does not have an item, but
`character2` does:

```
/package_id/characters/
├─character1/
│ └─character.png
└─character2/
  ├─character.png
  ├─item.png
  ├─item_done.png
  └─item_using.png
```

### fruits/

This folder must be present if the package includes any fruits, and holds
their resources.

For each fruit, there must be a folder with the same name as the fruit
ID containing the following files:

| Name    | Size      | Description                              |
| ------- | --------- | ---------------------------------------- |
| `0.png` | 500 ⨯ 500 | The fruit, not at all eaten.             |
| `1.png` | 500 ⨯ 500 | The fruit, eaten 1/4 of the way through. |
| `2.png` | 500 ⨯ 500 | The fruit, eaten 1/2 way through.        |
| `3.png` | 500 ⨯ 500 | The fruit, eaten 3/4 of the way through. |

**All** files must be present.

Example structure with two fruits, `fruit1` and `fruit2`:

```
/package_id/fruits/
├─fruit1/
│ ├─0.png
│ ├─1.png
│ ├─2.png
│ └─3.png
└─fruit2/
  ├─0.png
  ├─1.png
  ├─2.png
  └─3.png
```