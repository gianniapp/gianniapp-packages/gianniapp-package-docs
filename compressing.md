## Creating a compressed package file

Packages have a [file structure](structure.md) and must be made into a single
file for them to be easily distributed.

Packages can be compressed into a file with the `.gianniapppkg` extension.

To create one of these files, the file structure is compressed to a standard zip
file which is then renamed to have the `.gianniapppkg` extension.

The compressed zip file must have a single folder in its top level directory,
with the same name as the package ID, and, inside of it, all the package's files
such as `meta.json`, `icon.png` and the `characters` and `fruits` folders, if
present.

### Distribution quirks

When serving `.gianniappkg` files from a web server, it is recommended to use
the `application/x-gianniapp-package` MIME type to avoid browsers automatically
decompressing the file and/or adding the `.zip` extension to the end of the
filename.