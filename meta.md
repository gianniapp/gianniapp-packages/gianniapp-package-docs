## meta.json

This file contains basic information about the package in JSON format.

| Key            | Value                                 | Description                                                                                                   |
| -------------- | ------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| `pkgVersion`   | `int` 2                               | Version of this documentation that the package adheres to.                                                    |
| `version`      | `string`                              | Version of the package. It is recommended to use the x.x.x format (i.e. `1.0.0`)                              |
| `name`         | `string` or [`Localized`](#Localized) | Name of the package.                                                                                          |
| `description`  | `string` or [`Localized`](#Localized) | Description of the package.                                                                                   |
| `author`       | [`Author`](#Author)                   | Details about the package author.                                                                             |
| `fruits`       | [`Fruits`](#Fruits)                   | Defines the fruits included with the package. _Only required if the package includes any fruits._             |
| `characters`   | [`Characters`](#Characters)           | Defines the characters included with the package. _Only required if the package includes any characters._     |
| `achievements` | [`Achievements`](#Achievements)       | Defines the achievements included with the package. _Only required if the package includes any achievements._ |

### Author

An `object` that contains author information.

Only the `name` key is required.

| Key       | Value                                 | Description              |
| --------- | ------------------------------------- | ------------------------ |
| `name`    | `string` or [`Localized`](#Localized) | Author's name.           |
| `email`   | `string`                              | Author's email address.  |
| `website` | `string`                              | URL to author's website. |

Example:

```json
{
  "name": "gianni mangia una mela",
  "email": "info@gianni.app",
  "website": "https://gianni.app"
}
```
### Achievement

An `object` that defines an achievement.

| Key             | Value                                                                 | Description                                                                        |
| --------------- | --------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| `name`          | `string` or [`Localized`](#Localized)                                 | Name of the achievement.                                                           |
| `description`   | `string` or [`Localized`](#Localized)                                 | Description of the achievement.                                                    |
| `visible`       | `boolean`                                                             | If true, the achievement details will be hidden until the achievement is unlocked. |
| `conditionSets` | `array` of `array` of [`AchievementCondition`](#AchievementCondition) | Defines the conditions necessary to unlock the achievement.                        |

Each array of [`AchievementCondition`](#AchievementCondition) inside the `conditionSets` key defines one condition set.

If all of the conditions within a set are met, the achievement will be unlocked.

Example 1: An achievement that is unlocked when the user purchases either the `banana` OR `watermelon` fruit inside the `gianniapp` package.

```json
{
  "visible": true,
  "name": "Summer",
  "description": "Freshen up with a watermelon or a banana.",
  "conditionSets": [
    [
      {
        "type": "purchase",
        "purchaseType": "fruit",
        "id": "gianniapp/watermelon"
      }
    ],
    [
      {
        "type": "purchase",
        "purchaseType": "fruit",
        "id": "gianniapp/banana"
      }
    ]
  ]
}
```

Example 2: An achievement that is unlocked when the user purchases both the `banana` AND `watermelon` fruits inside the `gianniapp` package.

```json
{
  "visible": true,
  "name": "Summer combo",
  "description": "Freshen up significantly with a watermelon and a banana.",
  "conditionSets": [
    [
      {
        "type": "purchase",
        "purchaseType": "fruit",
        "id": "gianniapp/watermelon"
      },
      {
        "type": "purchase",
        "purchaseType": "fruit",
        "id": "gianniapp/banana"
      }
    ]
  ]
}
```

Notice how in the first example, `conditionSets` contains two arrays, each with
a single [`AchievementCondition`](#AchievementCondition) inside, while in the
second example, `conditionSets` contains a single array with two
[`AchievementCondition`](#AchievementCondition)s.

### AchievementCondition

An `object` that defines a condition for achivement unlocking.

The only fixed key is `type`, which defines what type of check is performed.  
The other keys depend on the value of `type`.

The valid values for `type` are listed below, along with the other keys that
must be in the `object` when that `type` is used.

#### fruitcount

This condition is true when the user has a particular amount of fruits.

| Key      | Value    | Description                                                                         |
| -------- | -------- | ----------------------------------------------------------------------------------- |
| `target` | `int`    | The amount of fruits that must be obtained.                                         |
| `id`     | `string` | The ID of the fruit that the `target` refers to, in the format `fruitid/packageid`. |

#### level

This condition is true when the user has reached a particular level.

| Key       | Value     | Description                                                                   |
| --------- | --------- | ----------------------------------------------------------------------------- |
| `target`  | `int`     | The level that must be reached.                                               |
| `vanilla` | `boolean` | If true, the vanilla mode level will be checked instead of the regular level. |

#### purchase

This condition is true when the user has made a specific purchase in the _gianniShop_.

| Key            | Value    | Description                                                                   |
| -------------- | -------- | ----------------------------------------------------------------------------- |
| `purchaseType` | `string` | The type of item that must be purchased. The only valid value is `fruit`.     |
| `id`           | `string` | The ID of the item that must be purchased, in the format `fruitid/packageid`. |

#### vanillacount

This condition is true when the user has a particular amount of fruits in
vanilla mode.

| Key      | Value    | Description                                                                         |
| -------- | -------- | ----------------------------------------------------------------------------------- |
| `target` | `int`    | The amount of fruits that must be obtained.                                         |

### Achievements

An `object` that contains information on the achievements included with the
package.

The key names are the achievement IDs (such as `firstapple` or `level20`) and
must be unqiue within the package.

The values are the corresponding [`Achievement`](#Achievement) objects.

### Character

An `object` that defines a character.

The character image files are stored in the
[`characters`](structure.md#characters) folder.

| Key     | Value                                        | Description                                                                               |
| ------- | -------------------------------------------- | ----------------------------------------------------------------------------------------- |
| `name`  | `string` or [`Localized`](#Localized)        | Name of the character.                                                                    |
| `level` | `int`                                        | The level that must be reched to unlock the character.                                    |
| `item`  | [`CharacterItem`](#CharacterItem) or `null`  | Character's item. If the character does not have an item and instead eats fruits, `null`. |

If a character has `level` == `0`, then the player will be immediately able to
use it.

Example of a character without an item:

```json
{
  "name": "gianni",
  "level": 0,
  "item": null
}
```

Example of a character with an item:

```json
{
  "name": "gisella",
  "level": 20,
  "item": {
    "name": {
      "en": "Toaster",
      "it": "Tostapane",
      "la": "Tosti panis creator"
    },
    "duration": 5000,
    "reward": {
      "coin": 20,
      "gianniapp/apple": 30
    }
  }
}
```

### CharacterItem

An `object` that defines a character's item.

Characters can either eat fruits (i.e. `gianni`) or they can have an item linked
to them which, when used, grants a reward to the user after a set amount of time
(i.e. `gisella`).

| Key        | Value                                 | Description                                                           |
| ---------- | ------------------------------------- | --------------------------------------------------------------------- |
| `name`     | `string` or [`Localized`](#Localized) | Name of the item.                                                     |
| `duration` | `int`                                 | How long the player must wait to receive the reward, in milliseconds. |
| `reward`   | [`CharacterReward`](#CharacterReward)   | The reward given to the user once the item has been used.           |

### CharacterReward

An `object` that defines the reward for using a
[`CharacterItem`](#CharacterItem).

The key names are the fruit IDs (such as `gianniapp/apple`) and the values are
`int`s that define the amount to give the user. The rewards will be given even
if the user hasn't purchased the corresponding fruit yet.

For example, to give 30 apples as a reward, the key `gianniapp/apple` must have
the value `30`.

To give coins as rewards, set the `coin` key.

The first fruit in this object (excluding the `coin` key) will be displayed on
the top left of the game window along with its total, even if that fruit hasn't
been purchased yet.

Example:

```json
{
  "coin": 20,
  "gianniapp/pear": 40,
  "gianniapp/apple": 30
}
```

In this exaple, once the player has used the [`CharacterItem`](#CharacterItem)
they will receive 20 _gianniCoins_, 40 pears and 30 apples.

On the top left of the game window, the user will see how many pears they have.

### Characters

An `object` that contains information on the characters included with the
package.

The key names are the character IDs (such as `gianni` or `gisella`) and must be
unqiue within the package.

The values are the corresponding [`Character`](#Character) objects.

### Fruit

An `object` that defines a fruit.

The fruit image files are stored in the [`fruits`](structure.md#fruits) folder.

| Key      | Value                                 | Description                                                                                                        |
| -------- | ------------------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| `cost`   | `int`                                 | The amount of gianniCoins a player must spend to buy the fruit.                                                    |
| `name`   | `string` or [`Localized`](#Localized) | Name of the fruit. (i.e. `Apple`)                                                                                  |
| `amount` | `object`                              | An object with two `string`s or [`Localized`](#Localized)s: `singular` (i.e. `apple`) and `plural` (i.e. `apples`) |

If a fruit has `cost` == `0`, then it will not be displayed in the _gianniShop_
and the player will be immediately able to eat it.

Example:

```json
{
  "cost": 0,
  "name": {
    "en": "Apple",
    "it": "Mela",
    "la": "Malum"
  },
  "amount": {
    "singular": {
      "en": "apple",
      "it": "mela",
      "la": "malum"
    },
    "plural": {
      "en": "apples",
      "it": "mele",
      "la": "mali"
    }
  }
}
```

### Fruits

An `object` that contains information on the frtuis included with the package.

The key names are the fruit IDs (such as `apple` or `pear`) and must be
unqiue within the package.

The values are the corresponding [`Fruit`](#Fruit) objects.

### Localized

An `object` used to localize a string in various languages.

Its keys are the language codes and the values are `string`s that consist of the
localized content.

Optionally, the key `default`, with the value being a `string` of a language
code, can be set to define which language's content should be displayed by the
client if it cannot find a `string` for its own language.

Example:

```json
{
  "default": "en",
  "en" : "English value",
  "it": "Valore in italiano"
}
```

In this example, if the client is set to English it will display
`English value`, if it is set to Italian it will display `Valore in italiano`,
and if it is set to a different language it will display the English string
(`English value`).

If the `default` key is not set and the client cannot find a `string` for its
own language, it will try to find keys for the following languages, in this
order:

- [`en`] English
- [`it`] Italian